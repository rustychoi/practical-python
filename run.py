#!/usr/bin/env python3
def another_random_function(xs):
    for i in range(len(xs)):
        xs[i] *= 2

def some_random_function(xs):
    another_random_function(xs)
    i = 2
    i += 1

#
xs = [1, 2, 3, 4]

# double the elements `xs`
some_random_function(xs)

# prints `[2, 4, 6, 8]`
print(xs)
