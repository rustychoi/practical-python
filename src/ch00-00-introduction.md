# Introduction

Welcome to _Practical Python_, an collection of helpful concepts that will allow you to write Python programs that are:
1. performant
1. efficient
1. clean
1. safe
