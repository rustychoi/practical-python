# Creating Sets

List comprehensions are not restricted to creating lists; it can be used to create sets too!

Given a list comprehension like the following:
```python
xs = [ expr for item in iterable if condition ]
```

You can turn this into a set comprehension by just using the curly braces:
```python
xs = { expr for item in iterable if condition }
```

That's all!
