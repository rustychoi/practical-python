# Practical Python

_by Jack Choi_

This is the first version of the text _Practical Python_.

This book is all about learning practical things specific to Python, to help you:
1. write a more efficient code
1. write a more performant code
1. write a safer code
1. write a cleaner code
