# Lists

A `list` is the most basic container type in not just Python, but practically any programming language you'll come across. It is nothing more than a sequence of items.

Here is the [official Python documentation on the `list` data type](https://docs.python.org/3/library/stdtypes.html#list).

Let's first explore how to work with a `list` in Python.

---

## Creating a `list`

Below are some of the ways you can create an empty `list`. While both ways work, it's more common to see people create one using square brackets like `[]`, because it's more concise and idiomatic:

```python
#
list_1 = []      # use the square brackets to create an empty list
list_2 = list()  # explicitly call the constructor

# prints `[] []`
print(list_1, list_2)
```

You can also create a list with elments alreday in it:

```python
#
list_with_items_in_it = [1, 3, 5, 3]

# prints `[1, 3, 5, 3]`
print(list_with_items_in_it)
```

---

## Adding items to `list`

Let's create a list that contains the numbers `1`, `3`, `2`, and `0`, by creating an empty list then using `.append`:

```python
# construct an empty list
my_list = []

# insert these numbers to the back of the list, in this order
my_list.append(1)  # after this line executes, my_list = [1]
my_list.append(3)  # after this line executes, my_list = [1, 3]
my_list.append(2)  # after this line executes, my_list = [1, 3, 2]
my_list.append(0)  # after this line executes, my_list = [1, 3, 2, 0]

# prints `[1, 3, 2, 0]`
print(my_list)
```

1. you may have noticed but `.append` is a method that inserts a given item to the ___back___ of the list
    1. this is the most common way to insert items into a `list`
    1. you should use this unless you know what you're doing

---

## Iterating through a `list`

The simplest and the easies way to iterate through a `list` is to iterate on the items directly:

```python
# the same list as before
my_list = [1, 3, 2, 0]

# iterate through the list from the first item to the last, in that order
for number in my_list:
    print(number)
# should print 1, 3, 2, 0, each on a different line
```

Another way to do this is to use `range` and index into the list:

```python
# the same list as before
my_list = [1, 3, 2, 0]

# use `i` in the range `[0, len(my_list))`
# in this case, `[0, 4)`
for i in range(len(my_list)):
    number = my_list[i]
    print(number)
# should print 1, 3, 2, 0, each on a different line
```

The above ways are identical to each other, but unless you're interested in the index associated with each element, prefer the first method.

> There is actually a way that combines both, which is to use `enumerate`:
> ```python
> # the same list as before
> my_list = [1, 3, 2, 0]
>
> #
> for index, number in enumerate(my_list):
>     print(index, number)
> # prints
> # 0 1
> # 1 3
> # 2 2
> # 3 0
> ```
>
> It essentially allows you to iterate through a list using _both_ the index and the element itself.

Note that the order of iteration through a `list` is identical to the order in which those elements appear in the `list`. This is subtle but important.

---

## Indexing

Indexing is also a crucial feature of a `list`, which allows you to access an item at a specific location/index:
```python
# the same list as before
my_list = [1, 3, 2, 0]

# print the first item in the list
first_item = my_list[0]
print(first_item)  # prints `1`

# print the last item in the list
last_item = my_list[ len(my_list) - 1 ]
print(last_item)  # prints `0`
```

---

## Summary

Use a `list` if:
1. you don't know what else to use
1. you want to just hold multiple pieces of data together in a container
1. you care about the iteration order
