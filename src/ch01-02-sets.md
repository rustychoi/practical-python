# Sets

A `set` is another popular container in Python, which serves a similar but different purpose to a `list`.

Here is the [official Python documentation on the `set` data type](https://docs.python.org/3/library/stdtypes.html#set).

Use a `set` if:
1. you want to hold multiple pieces of data together in a container
    1. you don't care about the order in which these data are kept in the said container
    1. the most frequently used operation is a membership check, in other words checking to see if a given item exists in the said container
    1. you don't need to store duplicate items in the container

---

## `set` Construction

This is the only way to create an empty `set` in Python:

```python
set_1 = set()
```

However if you are creating a set with elements in it already, you can use curly braces:
```python
set_with_items_in_it = {1, 3, 5, 3}

# print
print(set_with_items_in_it)  # IMPORTANT: ONLY PRINTS `3` ONCE
```
Notice an important thing here:
1. even though we put `3` in the set twice, it only appears once when we `print` it.
    1. if you are not okay with this, you should use a `list` instead

---

## Adding items to `set`

Let's create a list that contains the numbers `1`, `3`, `2`, and `0`, without using `x = {1, 3, 2, 0}`:
```python
# construct an empty set
my_set = set()

# insert these numbers to the back of the list, in this order
my_set.add(1)  # after this line executes, my_set = {1]
my_set.add(3)  # after this line executes, my_set = {1, 3}
my_set.add(2)  # after this line executes, my_set = {1, 3, 2}
my_set.add(0)  # after this line executes, my_set = {1, 3, 2, 0}

# will probably print `{0, 1, 2, 3}`
print(my_set)
```

So you basically use `.add` instead of `.append`.

---

## Iterating through `set`

Here is practically the only way to iterate through a `set`, which is to iterate directly using the elements:
```python
# the same set as before
my_set = {1, 3, 2, 0}

# iterate through the set from the first item to the last, in that order
for number in my_set:
    print(number)
# will print the elements in some order
```

Observe another important detail here:
1. when iterating through a `set`, the order in which the elements appear are __NOT__ guaranteed
    1. if the iteration order is important, you should not use a `set`
