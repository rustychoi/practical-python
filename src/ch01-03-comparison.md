# Comparison

This is the most important subchapter in this chapter, and it's all about when to use a `list` and when to use a `set`.

It practically comes down to 1 thing: the performance of ___membership checks___.

> It's a pretty common task in programming to see if a container contains some data that you are looking for. This operation is called a _membership check_, because you are checking to see if some data is a _member_ of the container of interest.
>
> You can perform a membership check in Python using the `in` keyword:
> ```python
> # using a list
> my_list = [1, 3, 4]
> print(3 in my_list)  # prints `True`
> 
> # using a set
> my_set = {1, 3, 4}
> print(5 in my_set)  # prints `False`
> ```

Let's write some code so that we can see the difference in the speed of membership checks using a `list` and a `set`.

---

## Using a `set`

The following code snippet simply does the following:
1. create an empty set
1. add all numbers between `0` to `10**8` into the set
    1. note that in Python, `10**8` \\( = 10^{8} = 100000000 \\)
1. perform a membership check by running `-1 in my_set`, which checks to see if the number `-1` exists in the set (it does not)
    1. see how long it takes to perform this membership check

On my machine, running this code prints `2.86102294921875e-06`, which essentially means that it took `0.00000286102294921875` seconds. That's about 3 microseconds, which is pretty fast!

Feel free to copy and run this code on your machine, to see it for yourself!
```python
import time

# empty set
my_set = set()

# add 0 through 10**8 in the set
for i in range(10**8):
    my_set.add(i)

#
start = time.time()  # clock time at this point
result = -1 in my_set
end = time.time()  # clock time at this point

# see how many seconds have passed between `start` and `end` 
elapsed_seconds = end - start
print(elapsed_seconds)
```

---

## Using a `list`

The following code block does essentially the same thing as the example above, except that instead of using a `set`, it uses a `list`.

This code printed `1.0303471088409424` on my machine.

```python
import time

# empty list
my_list = []

# add 0 through 10**8 in the list
for i in range(10**8):
    my_list.append(i)

#
start = time.time()  # clock time at this point
result = -1 in my_list
end = time.time()  # clock time at this point

# see how many seconds have passed between `start` and `end` 
elapsed_seconds = end - start
print(elapsed_seconds)
```

---

## Results

Simply put, we have the following result:

\\[ \frac{\texttt{using a list}}{\texttt{using a set}} = \frac{1.030347108840942}{0.0000028610229492187} \approx 360132.416667 \\]

In other words, given a `list` and a `set` that contains \\( 10^{8} = 100000000 \\) elements, a membership check using a `list` took __360132 times longer!!__ than using a `set`.

Understanding exactly why there is such a huge difference requires us to understand what is called a `hash function`, which we will cover in the near future.

For now, the takeaway is the following:
1. if your code requires you to perform lots of membership checks, `set` will outperform `list` in most scenarios
1. be careful about the fact that sets do not store duplicate elements
1. be careful about the fact that the order in which elements appear when iterating through a `set` is not consistent and you should not assume anything about the order

