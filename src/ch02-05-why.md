# Why List Comprehension

So what are some concrete benefits to using list comprehension?

---

## Performance

Often times, using list comprehension is much faster than using a traditional for-loop.

For example, consider the following code that inserts every number from `0` to `10**8` to a list, and we will time it to see how long this takes:

```python
import time

#
start = time.time()

#
xs = []
for x in range(10**8):
    xs.append(x)

#
end = time.time()

elapsed = end - start

# print how many elements are in the list
print(len(xs))
# elapsed tmie in seconds
print(elapsed)
```

On my machine, this prints `11.463484287261963`, meaning it took about `11.5` seconds.

Let's try the following example which does the same thing, except that it uses list comprehension:

```python
import time

#
start = time.time()

#
xs = [ x for x in range(10**8) ]

#
end = time.time()

elapsed = end - start

# print how many elements are in the list
print(len(xs))
# elapsed tmie in seconds
print(elapsed)
```

This took `4.830634832382202`, which is less than half the time it took to do this using a traditional for loop!

We won't go too deeply into why using list comprehension results in a faster code, but just know that often times the Python interpreter is able to add more optimizations to your Python code when using list comprehension.

---

## Conciseness

I won't talk too much about the conciseness aspect of list comprehension because you've seen examples already, and you may have seen that it simplifies the code a bit!

---

## Functional

Another benefit of using list comprehension is that it allows you to write code that is more [functional](https://en.wikipedia.org/wiki/Functional_programming) and [declarative](https://en.wikipedia.org/wiki/Declarative_programming). This will be a big topic for the future, but we won't explore it for now.

For now, the take-away is that list comprehension often reduces the number of mutations that are present in your program, and less mutations is often better because programs become easier to reason about.
