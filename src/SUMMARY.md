# Summary

[Practical Python](./title-page.md)
[Introduction](./ch00-00-introduction.md)

- [Lists and Sets](./ch01-00-lists-and-sets.md)
    - [Lists](./ch01-01-lists.md)
    - [Sets](./ch01-02-sets.md)
    - [Comparison](./ch01-03-comparison.md)
- [List Comprehension](./ch02-00-list-comprehension.md)
    - [Creating Lists: Basic](./ch02-01-creating-lists-basic.md)
    - [Creating Lists: Advanced](./ch02-02-creating-lists-advanced.md)
    - [Creating Sets](./ch02-03-sets.md)
    - [Creating Dictonaries](./ch02-04-dicts.md)
    - [Why List Comprehension](./ch02-05-why.md)
