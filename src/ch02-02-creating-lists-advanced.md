# Creating Lists: Advanced

Let's explore a slightly more advanced examples!

---

## Conditional Operation

> Recall the conditional operator in Python; the following two pieces of codes are identical:
> ```python
> n = 3
> if n % 2 == 0:
>     is_even = True
> else:
>     is_even = False
> ```
> ```python
> n = 3
> is_even = True if n % 2 == 0 else False
> ```
>
> Essentially, conditional operators have the following form:
> ```python
> x = if_true if cond else if_false
> ```
> 1. `x` takes on the value of `if_true` if `cond` evaluates to `True`
> 1. `x` takes on the value of `if_false` otherwise

Suppose we want to transform the following piece of code such that it uses list comprehension:
```python
#
xs = [1, 3, 4]

#
parities = []
for x in xs:
    if x % 2 == 0:
        parities.append('even')
    else:
        parities.append('odd')

# prints `['odd', 'odd', 'even']`
print(parities)
```

We make use of the conditional operator described above:
```python
#
xs = [1, 3, 4]

#
parities = [ 'even' if x % 2 == 0 else 'odd' for x in xs ]

# prints `['odd', 'odd', 'even']`
print(parities)
```

Let's see how this fits into the syntax we described before. We said that basic cases of list comprehension has the following form:
```python
ys = [ expression for item in iterable ]
```
1. `expression`: `'even' if x % 2 == 0 else 'odd'`
1. `item`: `x`
1. `iterable`: `xs`

Checks out!

---

## Filtering

Suppose that we want to filter a list so that it only contains the elements we want. Let's transform the following code to use list comprehension:
```python
#
xs = [1, 3, 4]

#
odds = []

#
for x in xs:
    if x % 2 == 1:
        odds.append(x)

# prints `[1, 3]`
print(odds)
```

We make use of a syntax that we have not seen yet:
```python
#
xs = [1, 3, 4]

#
odds = [ x for x in xs if x % 2 == 1 ]

# prints `[1, 3]`
print(odds)
```

To understand this, we need to update the syntax that we have been using for list comprehensions. The full syntax for list comprehension is:

```python
ys = [ expression for item in iterable if condition ]
```
1. `expression`: same as before
1. `item`: same as before
1. `iterable`: same as before
1. `condition`: only `item`'s that satisfy this `condition` will be considered
    1. note that `if condition` is optional and may not appear in list comprehensions

In the example from earlier, `odds = [ x for x in xs if x % 2 == 1 ]`, we have that:
1. `expression`: `x`
1. `item`: `x`
1. `iterable`: `xs`
1. `condition`: `x % 2 == 1`

Since only `1` and `3` in `xs` satisfied `condition`, only those were considered.

---

## Multiple Iterables

### Basic 

Suppose you want to create a list of tuples like so: `[(0, 0), (0, 1), (1, 0), (1, 1)]`.

The traditional way to do this would be:
```python
#
xs = []

#
for i in range(2):
    for j in range(2):
        xs.append((i, j))

# prints `[(0, 0), (0, 1), (1, 0), (1, 1)]`
print(xs)
```

Using list comprehension, you get:
```python
#
xs = [ (i, j) for i in range(2) for j in range(2) ]

# prints `[(0, 0), (0, 1), (1, 0), (1, 1)]`
print(xs)
```

### Advanced

What about a list of all tuples `(i, j)` such that:
1. `0 <= i < 3`
1. `0 <= j < 3`
1. `i < j`

Traditional:
```python
#
xs = []

#
for i in range(3):
    for j in range(3):
        if i < j:
            xs.append((i, j))

# prints `[(0, 1), (0, 2), (1, 2)]`
print(xs)
```

List comprehension:
```python
#
xs = [ (i, j) for i in range(3) for j in range(3) if i < j ]

# prints `[(0, 1), (0, 2), (1, 2)]`
print(xs)
```

> For readability, you can write it like this:
> ```python
> xs = [ (i, j) for i in range(3)
>               for j in range(3)
>               if i < j ]
> ```
