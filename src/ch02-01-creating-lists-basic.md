# Creating Lists: Basic

---

## Example 1: increment

Let's see some examples first, before we talk about the technicalities. In the following code block, we will try to take some list of numbers `xs`, and create another list where each element has been incremented by `1`. The traditional way to do this would be:
```python
# the numbers to increment
xs = [1, 3, 4]

# create an new list `ys`, to leave `xs` untouched
ys = []
for x in xs:
    ys.append(x + 1)

# prints `[2, 4, 5]`
print(ys)
```

Now let's do this exact same operation, but using list comprehension:
```python
# the numbers to increment
xs = [1, 3, 4]

# use list comprehension this time
ys = [ x + 1 for x in xs ]

# prints `[2, 4, 5]`
print(ys)
```

Much more concise, and this actually runs faster too!

---

## Example 2: function application

Let's do something similar, except that we are calling a function using the elements in `xs`:
```python
# strings to turn into floats
xs = ['0', '2', '4.13']

#
xs_floats = []
for x in xs:
    xs_floats.append(float(x))

# prints `[0.0, 2.0, 4.13]`
print(xs_floats)
```

Now let's do this exact same operation, but using list comprehension:
```python
# strings to turn into floats
xs = ['0', '2', '4.13']

#
xs_floats = [ float(x) for x in xs ]

# prints `[0.0, 2.0, 4.13]`
print(xs_floats)
```

---

## Pattern

Now let's lay out the syntax more concretely. For basic list comprehension, we have the following syntax:
```python
ys = [ expression for item in iterable ]
```
1. `expression`: an arbitrary Python expression that may make use of `item`, like `3`, `None`, `f(item)`, and `[1, 4]`
1. `item`: iteration variable used to refer to the items in `iterable`, just like in a `for` loop
1. `iterable`: an iterable python object, like a `list`, a `set`, or a `generator`

I like to draw parallels between list comprehensions and a tradition `for` loop, as this helps me understand what list comprehension does. The following two pieces of code are identical:
```python
ys = [ expression for item is iterable ]
```

```python
ys = []
for item in iterable:
    ys.append(expression)
```
