# Creating Dictonaries

You can create dictionaries using list comprehension too!

A dictionary comprehension is a bit different; recall this example of creating a set from earlier
```python
xs = { expr for item in iterable if condition }
```

Dictionary comprehension looks very similar:
```python
xs = { key: value for item in iterable if condition }
```

Suppose we want a dictionary like the following: `{0: 0, 1: 1, 2: 4, 3: 9}`. Essentially:
1. keys are in the range `[0, 4)`
2. values are the squares of the keys

Traditonally we would do this:
```python
# 
xs = {}
for i in range(4):
    xs[i] = i**2

# prints `{0: 0, 1: 1, 2: 4, 3: 9}`
print(xs)
```

Using list comprehension, we can do the following:

```python
# 
xs = { i: i**2 for i in range(4) }

# prints `{0: 0, 1: 1, 2: 4, 3: 9}`
print(xs)
```
