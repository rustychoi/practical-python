# List Comprehension

There are several ways to create lists in Python, but using _list comprehension_ provides simplicity, performance and conciseness.

In this chapter we will explore list comprehensions with examples, and talk about when to use it.
