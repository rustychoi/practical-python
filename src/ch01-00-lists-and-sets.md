# Lists and Sets

In this chapter we'll explore 2 of the most popular container data types in Python: `list` and `set`.

The goals of this chapter are to:
1. understand how to work with a `list` in Python
1. understand how to work with a `set` in Python
1. understand when one is better than the other
